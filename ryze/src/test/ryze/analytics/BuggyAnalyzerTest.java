package ryze.analytics;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class BuggyAnalyzerTest {

	
	@Test
	public void buggyanalyzer_nolines_all0inbuggyanalysis(){
		List<String> noLines = Arrays.asList(new String[]{});  //here is an example on how to create input corresponding to no input lines
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(noLines).get(0);
		assertEquals(0, lysis.getTotalLines());
	}
	
	@Test
	public void buggyanalyzer_threeLines_totalLinesIs3(){
		List<String> threeLines = Arrays.asList(new String[]{"hello", "beautiful", "world"}); // an example of short input
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(threeLines).get(0);
		assertEquals(3, lysis.getTotalLines());
	}

	@Test
	public void buggyanalyzer_multLines_resultExpected(){ //TODO change test name
		List<String> multLines = Arrays.asList(new String[]{ // an example of longer input
				"it",
				"was",
				"the",
				"best",
				"of",
				"times, it was the worst",
				"of times",
		});

		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(multLines).get(0);
		assertEquals(7, lysis.getTotalLines());
	}
	
	@Test
	public void buggyanalyzer_countovernum(){ //TODO change test name
		List<String> multLines = Arrays.asList(new String[]{ // an example of longer input
				"it,apple,banana,fk,you",
				"was,world,hello",
				"the,hi,how,are",
				"best,when,why,mine,laptop",
				"of",
				"times, it was the worst",
				"of times",
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(multLines).get(0);
		assertEquals(9, lysis.countlinesOvernum(multLines, 3));
	}

	@Test
	public void buggyanalyzer_wordDelimitation_resultExpected(){ //TODO change test name
		List<String> wordDelimitation = Arrays.asList(new String[]{
				"apple,books,bay,hi"
		});

		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(wordDelimitation).get(0); 
		assertEquals(4, lysis.getTotalWords());
	}

	@Test
	public void buggyanalyzer_wordCount_resultExpected(){ //TODO change test name
		//TODO Construct your own data 
		List<String> wordCount = Arrays.asList(new String[]{
				"apple,some,bay"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(wordCount).get(0); 
		assertEquals(3, lysis.getTotalWords());
	}

	@Test
	public void buggyanalyzer_totalWordCount_resultExpected(){ //TODO change test name
		//TODO Construct your own data 
		List<String> totalWordCount = Arrays.asList(new String[]{
				"apple,bay,some",
				"hi,kevin,how"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(totalWordCount).get(0); 
		assertEquals(6, lysis.getTotalWords());
	}

	@Test
	public void buggyanalyzer_avgLineLength_resultExpected(){ //TODO change test name
		//TODO Construct your own data 
		List<String> avgLineLen = Arrays.asList(new String[]{
				"bay,some,hi,new",
				"hi,old,why",
				"sydney,uni"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(avgLineLen).get(0); 
		//TODO you guys to do this
		assertEquals( 
			3, // TODO change to your_expected_value> 
			lysis.getAvgLineLength(),
			0.01 // we accept an error 0.01 and less
		);
		

	}
	@Test
	public void buggyanalyzer_minLineLength_resultExpected(){ //TODO change test name
		//TODO Construct your own data 
		List<String> minLineLen= Arrays.asList(new String[]{
				"bay,some,hi,new",
				"hi,old,why",
				"sydney,uni"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(minLineLen).get(0); 
		assertEquals(2, lysis.getMinLineLength());
	}
	@Test
	public void buggyanalyzer_maxLineLength_resultExpected(){ //TODO change test name
		//TODO Construct your own data 
		List<String> maxLineLen= Arrays.asList(new String[]{
				"bay,some,hi,new",
				"hi,old,why",
				"sydney,uni"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(maxLineLen).get(0); 
		assertEquals(4, lysis.getMaxLineLength());
	}
}
