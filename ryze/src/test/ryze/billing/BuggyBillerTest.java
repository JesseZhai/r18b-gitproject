package ryze.billing;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import ryze.analytics.BuggyAnalysis;

public class BuggyBillerTest {

	@Test
	public void buggybiller_allZeroAnalysis_0(){
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(0,0,0,0,0.0);
		assertEquals(0.0, bb.bill( Arrays.asList(lysis)).get(0), 0.01 );
	}

	@Test
	public void buggybiller_lowerRate_360(){ //TODO change test name
		BuggyBiller bb = new BuggyBiller(1,10);
		BuggyAnalysis lysis = new BuggyAnalysis(100,100,20,10,18);
		assertEquals(1000, bb.bill( Arrays.asList(lysis)).get(0), 0.01 );
	}
	
	@Test
	public void buggybiller_higherRate_2170() {//TODO change test name
		BuggyBiller bb = new BuggyBiller(1,10);
		BuggyAnalysis lysis = new BuggyAnalysis(10,110,110,110,110);
		assertEquals(10, bb.bill( Arrays.asList(lysis)).get(0), 0.01 );
}
}