package ryze.query;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class BuggyFileTextQueryTest {

	
	@Test
	public void buggyFileTextQuery_10xUniversity_10Lines() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/10x_university.txt");
		List<String> res = query.query("university");
		assertEquals(10, res.size());
	}
	

	@Test
	public void buggyFileTextQuery_10xUniversity1xComment_9Lines() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/10x_university_1x_comment.txt");
		List<String> res = query.query("university");
		assertEquals(9, res.size());
	}
	@Test
	public void buggyFileTextQuery_10xUniversity1stComment_9Lines() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/10x_university_1st_comment.txt");
		List<String> res = query.query("university");
		assertEquals(9, res.size());
	}

	@Test
	public void buggyFileTextQuery_10xUniversityLastComment_9Lines() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/10x_university_last_comment.txt");
		List<String> res = query.query("university");
		assertEquals(9, res.size());
	}

	@Test
	public void buggyFileTextQuery_10xUniversity1stAndLastComment_8Lines() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/10x_university_1st_and_last_comment.txt");
		List<String> res = query.query("university");
		assertEquals(8, res.size());
	}
	
	
	@Test
	public void buggyFileTextQuery_whitespaceb4comment_resultexpected() { //TODO rename test
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/whitespcaceb4comment.txt");
		List<String> res = query.query("university");
		assertEquals(4, res.size());
	}
	
	@Test
	public void buggyFileTextQuery_emptyfile_0Lines() {
		BuggyFileTextQuery query = new BuggyFileTextQuery();
		query.setFileName("data/emptytxt");
		List<String> res = query.query("university");
		assertEquals(0, res.size());
	}	
}
