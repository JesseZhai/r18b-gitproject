package ryze.workflow;

public class TypeMismatchException extends Exception {

	private static final long serialVersionUID = 4864270565413372376L;
	
	Class<?> output, input;

	public TypeMismatchException(Class<?> output, Class<?> input){
		
		super("Output type of " + output.getName() + " incompatible with input type of " + input.getName());
		this.output = output;
		this.input = input;
	}

	public Class<?> getOutput() {
		return output;
	}
	public Class<?> getInput() {
		return input;
	}

}
