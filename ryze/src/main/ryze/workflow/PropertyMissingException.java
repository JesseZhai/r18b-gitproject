package ryze.workflow;

public class PropertyMissingException extends Exception {

	public PropertyMissingException() {
		super();
	}
	
	public PropertyMissingException(String string) {
		super(string);
	}

}
