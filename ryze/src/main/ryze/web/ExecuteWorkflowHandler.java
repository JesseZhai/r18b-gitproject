package ryze.web;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.rythmengine.Rythm;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;
import fi.iki.elonen.NanoHTTPD.ResponseException;
import ryze.workflow.Workflow;
import ryze.workflow.WorkflowInfo;

public class ExecuteWorkflowHandler implements Handler {

	private static Logger LOG = Logger.getLogger(ExecuteWorkflowHandler.class.getName());
	@Override
	public Response handle(Method m, String uri, IHTTPSession session) {
		//look up the workflow and load form to input configuration for it
		// 
		Map<String, String> files = new HashMap<>();
		try {
			session.parseBody(files);
		} catch (IOException ioe) {
			return NanoHTTPD.newFixedLengthResponse(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage());
        } catch (ResponseException re) {
            return NanoHTTPD.newFixedLengthResponse(re.getStatus(), NanoHTTPD.MIME_PLAINTEXT, re.getMessage());
        }
		
		Map<String, String> params = session.getParms();
		int wfIdx = -1;
		String idx = params.get("idxWorkflow");
		if(idx != null) wfIdx = Integer.parseInt(idx);
		
		LOG.log(Level.INFO, "Workflow Index" + wfIdx);
		LOG.log(Level.INFO, params.toString());
		
		
		if(wfIdx < 0) {
			LOG.log(Level.FINE, "Unspecified index for list of workflows");
			return NanoHTTPD.newFixedLengthResponse(Response.Status.BAD_REQUEST, NanoHTTPD.MIME_PLAINTEXT, "Unspecified index for list of workflows");
		}

		wfIdx--;  //set to 0-based index for List
		WorkflowService wfSvc = WorkflowService.getInstance();
		try {
			List<WorkflowInfo> wfs = wfSvc.getWorkflows();
			if(wfIdx >= wfs.size()){
				LOG.log(Level.FINE, "Invalid index for list of workflows; index provided: " + wfIdx);
				return NanoHTTPD.newFixedLengthResponse(Response.Status.BAD_REQUEST, NanoHTTPD.MIME_PLAINTEXT, "Invalid index for list of workflows");
			}
			
			WorkflowInfo wif = wfs.get(wfIdx); 
			
			String[] keywords = {"sydney", "university"};
			String kwStr = params.get("keywords");
			if(kwStr != null && kwStr.trim().length() > 0){
				keywords = kwStr.split("\\s+");
			}
			
			Workflow wf = wif.getWorkflow(); 
			//set Properties to configure the query, analyzer and biller
			setProperties(wif.getQueryName(), wf.getQuery(), params);
			setProperties(wif.getAnalyzerName(), wf.getAnalyzer(), params);
			setProperties(wif.getBillerName(), wf.getBiller(), params);

		    	
			List<String> quests = wf.getQuery().query(keywords);

			List<Integer> analysis = wf.getAnalyzer().analyze(quests);
			List<Double> bill = wf.getBiller().bill(analysis);
			
			String page = Rythm.render("workflow.html", wfIdx+1, wif.getName(), keywords, 
					wif.getQueryName(), wif.getAnalyzerName(), wif.getBillerName(),
					getConfigFields(wf.getQuery()), 
					getConfigFields(wf.getAnalyzer()), 
					getConfigFields(wf.getBiller()), 
					quests, analysis, bill);
			
			return NanoHTTPD.newFixedLengthResponse(page);
			
		} catch (Exception e) {
			e.printStackTrace();
			return NanoHTTPD.newFixedLengthResponse(Response.Status.INTERNAL_ERROR, null, null, -1);
		}
		
		
	}

	private void setProperties(String name, Object obj, Map<String, String> params){
		Map<String,Object> fields = getConfigFields(obj);
		java.lang.reflect.Method[] methods = obj.getClass().getMethods();
FIELD:
		for(String baseName: fields.keySet()){
			String setter = "set" + baseName;
			String paramName = name + "_" + baseName;
			String paramVal = params.get(paramName);
			if(paramVal != null){
				for(java.lang.reflect.Method m: methods){
					if(m.getName().equals(setter)){
						//check if paramVal can fit the type
						Class[] args = m.getParameterTypes();
						if(args.length != 1){
							LOG.log(Level.INFO,  String.format("Setter %s.%s has too many arguments", name, setter));
						}else{
							Class type = args[0];
							Object val = paramVal;
							
							if(Integer.class.equals(type) || int.class.equals(type)){
								//try to convert user input to Integer
								try{
									val = new Integer(paramVal);
								}catch(NumberFormatException nfe){
									LOG.log(Level.SEVERE, String.format("%s expected as an Integer; input provided as %s", paramName, paramVal.toString() ) );
								}
							}else if (Long.class.equals(type) || long.class.equals(type)){
								try{
									val = new Long(paramVal);
								}catch(NumberFormatException nfe){
									LOG.log(Level.SEVERE, String.format("%s expected as a Long; input provided as %s", paramName, paramVal.toString() ) );
								}
							}else if (Short.class.equals(type) || Short.class.equals(type)){
								try{
									val = new Short(paramVal);
								}catch(NumberFormatException nfe){
									LOG.log(Level.SEVERE, String.format("%s expected as a Short; input provided as %s", paramName, paramVal.toString() ) );
								}
							}else if (Float.class.equals(type) || long.class.equals(type)){
								try{
									val = new Float(paramVal);
								}catch(NumberFormatException nfe){
									LOG.log(Level.SEVERE, String.format("%s expected as a Float; input provided as %s", paramName, paramVal.toString() ) );
								}
							}else if (Number.class.isAssignableFrom(type) 
									|| double.class.equals(type)
							){
								
								//try to convert user input to Double as a general number
								try{
									val = new Double(paramVal);
								}catch(NumberFormatException nfe){
									LOG.log(Level.SEVERE, String.format("%s expected as a Number; input provided as %s", paramName, paramVal.toString() ) );
								}
							}else if(CharSequence.class.isAssignableFrom(type)){
								val = paramVal.toString();
							}else{
								LOG.log(Level.SEVERE, String.format("Type for %s could not be determined. %s parameter will be ignored ", paramName, paramVal.toString() ) );
								continue; // go to next method 
							}
							
							try {
								m.invoke(obj, val); // set the property
								continue FIELD; //go to next field
							} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
								LOG.log(Level.SEVERE, String.format("Could not set %s using value %s", paramName, paramVal.toString() ) );
							}
						}
					}
				}
			}
		}
	}
	
	private Map<String,Object> getConfigFields(Object obj){
		
		Map<String, Object> fields = new HashMap<>();
		java.lang.reflect.Method[] methods = obj.getClass().getMethods();
		for(java.lang.reflect.Method m: methods){
			String name = m.getName();
			if(name.startsWith("set")){
				String baseName = name.replaceFirst("set", "");
				String getterName = "get"+baseName;

				try {
					
					java.lang.reflect.Method getter = obj.getClass().getMethod(getterName, new Class[]{});
					Object res = getter.invoke(obj, new Object[]{});
					fields.put(baseName, res);
					
					
				} catch (NoSuchMethodException e) {
					LOG.log(Level.SEVERE, "No getter method found for setter " + name);
				} catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					LOG.log(Level.SEVERE, "No access to getter method for setter " + name, e);
				}
			}
		}
		return fields;
	}
}
