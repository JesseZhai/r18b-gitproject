package ryze.web;

import ryze.workflow.PropertiesWorkflowLoader;

import java.time.LocalDateTime;
import java.util.List;

import ryze.workflow.WorkflowInfo;

public class WorkflowService {
	
	private static WorkflowService wfs;
	// clearly not thread-safe
	public static WorkflowService getInstance(){
		if(wfs == null){
			wfs = new WorkflowService();
		}
		return wfs;
	}

	private WorkflowService(){}
	
	private List<WorkflowInfo> workflows;
	private LocalDateTime loadTimestamp;
	
	public List<WorkflowInfo> getWorkflows() throws Exception{
		return getWorkflows(false);
	}

	public List<WorkflowInfo> getWorkflows(boolean reload) throws Exception{
		if(reload || workflows == null){
			loadWorkflows();
		}
		return workflows;
	}
	
	public LocalDateTime getLoadTime(){
		return loadTimestamp;
	}
	
	private void loadWorkflows() throws Exception{
		workflows = PropertiesWorkflowLoader.loadWorkflows();
		loadTimestamp = LocalDateTime.now();
	}
}
