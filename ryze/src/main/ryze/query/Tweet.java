package ryze.query;

import twitter4j.HashtagEntity;
import twitter4j.MediaEntity;
import twitter4j.Place;
import twitter4j.Status;
import twitter4j.User;

public class Tweet {

	public static final String UNKNOWN_COUNTRY = "NA";
	public static final String UNKNOWN_LOCATION = "NA";

	private int retweetCount = 0;

	private String text = "";
	private String country = UNKNOWN_COUNTRY;
	private String location = UNKNOWN_LOCATION;
	private String username = "";
	private String profileImgUrl = "";
	private String[] hashTags = new String[0];
	private String[] imgUrls = new String[0];
	
	public Tweet(Status status){
		User user = status.getUser();
		
		username = user.getName();
		
		profileImgUrl = user.getProfileImageURL();

		retweetCount = status.getRetweetCount();
		text = status.getText();
		Place pl = status.getPlace();
		if(pl != null){
			country = pl.getCountry();
			location = pl.getFullName();
		}
		if(country == null) country = UNKNOWN_COUNTRY;
		if(location == null) location = UNKNOWN_LOCATION;
		
		HashtagEntity[] hashtags = status.getHashtagEntities();			
		hashTags = new String[hashtags.length];
		int i=0;
		for(HashtagEntity tag: hashtags){
			hashTags[i++] = tag.getText();
		}
		
		MediaEntity[] imgs = status.getMediaEntities();
		int count=0;
		for(MediaEntity img: imgs){
			if(img.getType().equalsIgnoreCase("photo")){
				count++;
			}
		}
		imgUrls = new String[count];
		i = 0;
		
		for(MediaEntity img: imgs){
			if(img.getType().equalsIgnoreCase("photo")){
				imgUrls[i++] = img.getURL();
			}
		}
	}
	
	public String getUsername() {
		return username;
	}

	public String getProfileImageUrl() {
		return profileImgUrl;
	}

	public int getNumberOfRetweet() {
		return retweetCount;
	}

	public String getText() {
		return text;
	}

	public String getCountry() {
		return country;
	}

	public String getLocation() {
		return location;
	}

	public String[] getHashTags() {
		return hashTags;
	}

	public String[] getImageUrls() {
		return imgUrls;
	}
}
 