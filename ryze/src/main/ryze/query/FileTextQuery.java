package ryze.query;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
/**
 * Reads lines of text from a file AS-IS and returns those with matching keywords.
 * @author whwong
 *
 */
public class FileTextQuery implements Query<String>{

	private static Logger LOG = Logger.getLogger(FileTextQuery.class.getName());
	
	public static final String DEFAULT_FILE = "data/war_and_peace.txt";
	
	private String fileName =  DEFAULT_FILE;
	
	private List<String> lines;
	
	public FileTextQuery(){
		loadFile();
	}
	
	public FileTextQuery(String file){
		setFileName(file);
	}
	
	private void loadFile(){
		try (Scanner scanner = new Scanner(new File(fileName))){
			LinkedList<String> lines = new LinkedList<>();
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				lines.add(line);
			}
			
			this.lines = lines;
			
			
		} catch (FileNotFoundException e) {
			LOG.log(Level.SEVERE, fileName + " cannot be found.",  e);
		}
	}
	
	public void setFileName(String name){
		fileName = name;
		loadFile();
	}
	
	public String getFileName(){
		return fileName;
	}
	
	@Override
	public List<String> query(String... keywords) {
		if(keywords == null || keywords.length == 0){
			return lines.stream().collect(Collectors.toList());
		}else{
			return lines.stream()
					.filter(s -> contains(s, keywords))
					.collect(Collectors.toList());
		}
	}

	boolean contains(String line, String... keywords){
		for(String word: keywords){
			Pattern ptrn = Pattern.compile("\\b"+word+"\\b", Pattern.CASE_INSENSITIVE);
			Matcher m = ptrn.matcher(line);
			if(m.find()) return true;
		}
		return false;
	}
	
	
}
