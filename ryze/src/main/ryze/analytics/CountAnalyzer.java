package ryze.analytics;

import java.util.LinkedList;
import java.util.List;

public class CountAnalyzer  implements Analyzer<String, Integer>{

/**
 * Simple analyzer that counts number of tweets
 */
	@Override
	public List<Integer> analyze(List<String> tweets) {
		LinkedList<Integer> res = new LinkedList<>();
		res.add(tweets.size());
		return res;
	}

}
